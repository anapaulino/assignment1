/* Add Author and Project Details here 
Name: Ana Carolina dos Santos Paulino
Description of the repository/project:
Introduction to embedded systems software and dev. - Coursera
Week 1 application assignment
Analysis of arrays of unsigned char data items and execution of the following actions:
- Maximum, minimum, mean, and median of the data set;
- Sort the data set from large to small and vice-versa;
- Print that data to the screen in nicely formatted presentation.
*/
