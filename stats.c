/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c
 * @brief Running and displaying statistics about a given test vector
 *
 * Analysis of arrays of unsigned char data items and execution of the following actions:
 * - Maximum, minimum, mean, and median of the data set;
 * - Sort the data set from large to small and vice-versa;
 * - Print that data to the screen in nicely formatted presentation.
 *
 * @author Ana Carolina dos Santos Paulino
 * @date October 2020
 *
 */



#include <stdio.h>
#include <stdlib.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

int main(void) {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */
  /* Statistics and Printing Functions Go Here */
  
  print_array(test, SIZE);
  print_statistics(test, SIZE);
  sort_array(test, SIZE, 0); //Change 0 to 1 to sort in ascending order
  printf("Sorted array - ");
  print_array(test, SIZE);
  
  return 0;
}

/* Add other Implementation File Code Here */


void print_statistics(unsigned char * array, int size_array){
  printf("Median: %f\n", find_median(array, size_array));
  printf("Mean: %f\n", find_mean(array, size_array));
  printf("Maximum: %d\n", array[find_maximum(array, size_array)]);
  printf("Minimum: %d\n", array[find_minimum(array, size_array)]);
}

void print_array(unsigned char * array, int size_array){
  int i; 
  
  printf("Array contents: [");
  
  for ( i = 0; i < size_array; i++ ){
    printf( "%d ", array[i] );
  }

  printf( "]\n" );
}


float find_median(unsigned char * array, int size_array){
  unsigned char *sorted_array = NULL;
  float output = 0;
  int i;

  //Copy the original vector
  sorted_array = malloc(size_array * sizeof(unsigned char));

  for ( i = 0; i < size_array; i ++ ){
    sorted_array[i] = array[i];
  }

  sort_array(sorted_array,size_array,1); //In ascending order

  if (size_array%2 == 0){ //This means that there is no central element in the sorted vector
    output = (float)(sorted_array[(int) size_array/2 -1] + sorted_array[(int) size_array/2])/2;
  } else {
    output = sorted_array[(int) (size_array/2)];
  }

  free(sorted_array);
  return output;
}


float find_mean(unsigned char * array, int size_array){
  int i;
  int temp=0;

  if (size_array > 0){
    for ( i = 0; i < size_array; i++ ){
      temp+=array[i];
    }
    return (float) temp/size_array;
  } else {
    return 0;
  }
}


unsigned char find_maximum(unsigned char * array, int size_array){
  int i; 
  unsigned char temp=array[0], output=0;
  
  for ( i = 0; i < size_array; i++ ){
    if (array[i]>temp){
      temp=array[i];
      output=i;
    }
  }
  return output;
}


unsigned char find_minimum(unsigned char * array, int size_array){
  int i; 
  unsigned char temp=array[0], output=0;
  
  for ( i = 0; i < size_array; i++ ){
    if (array[i]<temp){
      temp=array[i];
      output=i;
    }
  }
  return output;
}


void sort_array(unsigned char * array, int size_array, unsigned char ascending){  
  unsigned char index = 0, temp = 0;
  int i;
  
  if (size_array>1){
    if (ascending) {
      index = find_minimum(array, size_array);
    } else {
      index = find_maximum(array, size_array);
    }

    //Swap array[index] and array[i]
    temp = array[0];
    array[0] = array[index];
    array[index] = temp;

    sort_array(&(array[1]), size_array-1, ascending);
  }
}
