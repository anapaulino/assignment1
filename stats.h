/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h
 * @brief Running and displaying statistics about a given test vector
 *
 * Analysis of arrays of unsigned char data items and execution of the following actions:
 * - Maximum, minimum, mean, and median of the data set;
 * - Sort the data set from large to small and vice-versa;
 * - Print that data to the screen in nicely formatted presentation.
 *
 * @author Ana Carolina dos Santos Paulino
 * @date October 2020
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/* Add Your Declarations and Function Comments here */ 

/**
 * @brief The main function
 *
 * The main function contains the array over which all the calculations will be perfoermed. It also call all the other functions.
 *
 * @param none
 *
 * @return zero
 */
int main(void);

/**
 * @brief Function that prints statistical information about a given array.
 *
 * This function formats and prints statistical information (minimum, maximum, mean and median) of an array.
 *
 * @param array address of the first position of an array
 * @param size_array size of the array
 *
 * @return none
 */
void print_statistics(unsigned char * array, int size_array);

/**
 * @brief Function that prints a given array.
 *
 * This function receives the first address of an array and its size, and prints it.
 *
 * @param array address of the first position of an array
 * @param size_array size of the array
 *
 * @return none
 */
void print_array(unsigned char * array, int size_array);

/**
 * @brief This function finds the median of an array.
 *
 * This function receives the first address of an array and its size, and finds its median.
 *
 * @param array address of the first position of an array
 * @param size_array size of the array
 *
 * @return the median of the array.
 */
float find_median(unsigned char * array, int size_array);

/**
 * @brief This function calculates the mean of an array.
 *
 * This function receives the first address of an array and its size, and calculates its mean.
 *
 * @param array address of the first position of an array
 * @param size_array size of the array
 *
 * @return the mean of the array.
 */
float find_mean(unsigned char * array, int size_array);

/**
 * @brief This function finds the maximum value of an array.
 *
 * This function receives the first address of an array and its size, and finds its maximum value. It returns the position of the maximum value of the array.
 *
 * @param array address of the first position of an array
 * @param size_array size of the array
 *
 * @return the position of the maximum value of the array.
 */
unsigned char find_maximum(unsigned char * array, int size_array);

/**
 * @brief This function finds the minimum value of an array.
 *
 * This function receives the first address of an array and its size, and finds its minimum value. It returns the position of the minimum value of the array.
 *
 * @param array address of the first position of an array
 * @param size_array size of the array
 *
 * @return the position of the minimum value of the array.
 */
unsigned char find_minimum(unsigned char * array, int size_array);

/**
 * @brief This function sorts an array.
 *
 * This function receives the first address of an array and its size, and sorts it in ascending or descending order according to the user.
 *
 * @param array address of the first position of an array
 * @param size_array size of the array
 * @param ascending boolean variable that indicates ascending order when true
 *
 * @return none
 */
void sort_array(unsigned char * array, int size_array, unsigned char ascending);

#endif /* __STATS_H__ */
